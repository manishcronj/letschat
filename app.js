var express=require('express');
var app=express();
var http=require('http');
var server=http.Server(app);
var io1=require('socket.io')(server);
var users=[];
var rooms=[];

app.use(express.static('chat'));

app.get('/',function(req,res){
	res.sendFile(__dirname+'/chat/templates/index.html');
});

var io=io1.of('/chat');
// Server section

io.on('connection',function(socket){

console.log('a new connection');
socket.on('disconnect',function(){
  
    for(var findSocket=0;findSocket<users.length;findSocket++)
    {
        if(socket.id==users[findSocket].userId)
      { 
        console.log(users[findSocket].userName+' : removed');
        users.splice(findSocket,1);
        break;
        }
    }
    io.emit('userlogout',users);
    console.log('disconnect');
});




socket.on('createRoom',function(participantsArray,roomName){
   var adminSocket=io.connected[socket.id];
   var isRoomExist=false;
   
   adminSocket.join(roomName);
    for(var participant=0;participant<participantsArray.length;participant++)
     {
        var participantSocket=io.connected[participantsArray[participant].userId];
        participantSocket.join(roomName);
    }

  var id;
  for(var isNameExist=0;isNameExist<rooms.length;isNameExist++)
  {
    if(roomName==rooms[isNameExist].roomName)
       {
           id=rooms[isNameExist].roomId;
           isRoomExist=true;
           break;
        // id=rooms[isNameExist].roomId;
        // for(var isIdExist=0;isIdExist<rooms.length;isIdExist++)
        // {
        //     if(id==rooms[isIdExist].roomId)
        //     {   
        //         id=
        //         isRoomExist=true;
        //         break;
        //     }
        // }
        // if(isRoomExist)
        //     break;
       } 
    

  }
   

     if(!isRoomExist)
     {
     var roomId=randomId(5);
     var room={roomId:roomId,roomName:roomName};
     rooms.push(room);

     }
     else
     {
     roomId=id;   
     var room={roomId:roomId,roomName:roomName};
     }
     io.to(roomName).emit('group',room);

   console.log(io.adapter.rooms); 
   console.log(roomName);
   console.log(participantsArray);

});

function randomId(length){

var chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
   var str='';
   for(var makeRandom=0;makeRandom<length;makeRandom++)
   {
    str += chars[Math.floor(Math.random() * chars.length)];
   }
  return str;
}
// socket.on('groupparticipants',function(participantsArray){

//     for(var participant=0;participant<participantsArray.length;participant++)
//     {

//     }
// });
socket.on('grpmsgsend',function(data){
   var senderName;
   for(var findSenderName=0;findSenderName<users.length;findSenderName++)
    {
        if(socket.id==users[findSenderName].userId)
        {
            senderName=users[findSenderName].userName;
            break;
        }
    }

   for(var room=0;room<rooms.length;room++)
   {
    if(data.groupId==rooms[room].roomId)

      socket.broadcast.to(rooms[room].roomName).emit('grpmsgrecieve',{msg:data.msg,groupId:rooms[room].roomId,senderName:senderName});
   }

});







socket.on('saveuser',function(userName){
  var tempArray=[];
  
	users.push({userName:userName,userId:socket.id});
  io.emit('onlineusers',users,socket.id);
	
    console.log(users);
});


socket.on('messagetosend',function(data){
	var receiverId;
    var senderName;
    for(var findReceiver=0;findReceiver<users.length;findReceiver++)
    {
        if(data.receiverName==users[findReceiver].userName)
        {
            receiverId=users[findReceiver].userId;
            break;
        }
    }
    for(var findSenderName=0;findSenderName<users.length;findSenderName++)
    {
        if(socket.id==users[findSenderName].userId)
        {
            senderName=users[findSenderName].userName;
            break;
        }
    }
    var sentMessage={msg:data.msg,receiverName:data.receiverName,senderName:senderName};
    io.to(receiverId).emit('messagetoreceive',sentMessage);//emmiting message  
   
 });

socket.on('showmsg',function(data){
  var receiverId;
  var senderName;
   for(var findReceiver=0;findReceiver<users.length;findReceiver++)
    {
        if(data.receiverName==users[findReceiver].userName)
        {
            receiverId=users[findReceiver].userId;
            break;
        }
    }

    for(var findSenderName=0;findSenderName<users.length;findSenderName++)
    {
        if(socket.id==users[findSenderName].userId)
        {
            senderName=users[findSenderName].userName;
            break;
        }
    }
    var data1={key:data.key,senderName:senderName};
    io.to(receiverId).emit('messagetoreceive',data1);

});

});


server.listen(3000,function(){
	console.log('server is listning on port 3000');
});
